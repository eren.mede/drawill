import Vue from 'vue';
import Router from 'vue-router';
import Home from "../router/Home.vue";
import Gallery from "../router/Gallery.vue";
import Pricing from "../router/Pricing.vue";
import ContactUs from "../router/ContactUs.vue";
import UpLoad from "../router/Upload.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/Gallery",
      name: "gallery",
      component: Gallery
      
    },
    {
      path: "/Pricing",
      name: "pricing",
      component: Pricing
      
    },

    {
      path: "/ContactUs",
      name: "contactus",
      component: ContactUs
      
    },

    {
      path: "/Upload",
      name: "upload",
      component: UpLoad
      
    }
  ]
});

